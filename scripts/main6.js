//   1
//   We can use backslash (\) to escape from a string and indicate that the next character 
// should be treated as a literal character rather than as a special character or string delimiter. 


//   2
// We can pronounce the function with two ways either using function declaration (function name () {};)
// or using function expression ( const name = function () {return};).

//  3
// Hoisting in JavaScript is a mechanism where variable and function declarations are moved to the top of
//  their respective scopes during the compilation phase of the JavaScript code, before the code is executed.
// In other words, even if you declare a variable or function after you use it in your code, JavaScript will
//  "hoist" it to the top of its scope so that it can be accessed anywhere in that scope.

// // get user input as a full date string
// const userInput = "2023-05-04";

// // create a new Date object with the user input
// const date = new Date(userInput);

// // get the year from the date object
// const year = date.getFullYear();

// console.log(year); // output: 2023
 //////////////////////////////////////////////////

// const year = date.getFullYear().toString().slice(-2);
// console.log(year); // output: 23

function createNewUser() {
    newUser = {
        firstName:prompt("What is your name?", "name"),
        lastName:prompt("What is your last name?", "Last name"),
        birthday:prompt("Please enter your birthday in the format DD/MM/YYYY:"),
        getAge() {
            const today = new Date();
            const dob = new Date (this.birthday);
            let age = today.getFullYear() - dob.getFullYear(this.birthday);
            const monthDiff = today.getMonth() - dob.getMonth();
            if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < dob.getDate())) {
                age--;
            }
            return age;
        }, 
        getPassword() {
            const userInput = this.birthday;
            const date = new Date(userInput);
            const year = date.getFullYear(this.birthday);
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.year;
        },
        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
          }
    }
    return newUser;
}


createNewUser();
let result = newUser.getLogin();
console.log(result);

let userAge = newUser.getAge();
console.log(userAge);

let password = newUser.getPassword();
console.log(password);










//Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() 
// та getPassword() створеного об'єкта.
//Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому 
// регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження.
// (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.

// Створити метод getAge() який повертатиме скільки користувачеві років.

// function getAge(birthdate) {
//     const today = new Date();
//     const birthDate = new Date(birthdate);
//     let age = today.getFullYear() - birthDate.getFullYear();
//     const monthDiff = today.getMonth() - birthDate.getMonth();
//     if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
//       age--;
//     }
//     return age;
//   }

// dateOfBirth:prompt(getFullYear()), setFullYear(year [, month, date])
// new Date(year, month, date, hours, minutes, seconds, ms)
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy)
// і зберегти її в полі birthday